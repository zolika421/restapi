<?php
class User
{
    // Connection instance
    private $_connection;
    // Config
    private $_config;
    // table name
    private $_table_name = "users";
    // Table columns
    public $uid;
    public $email;
    public $firstname;
    public $lastname;
    public $password;

    /**
     * Constructor
     *
     * Set config and DB connection
     *
     * @param object $connection PDO object
     */
    public function __construct($connection)
    {
        include 'config.php';
        $this->_config = $config;
        $this->connection = $connection;
    }

    /**
     * CRUD - create
     *
     * @return mixed boolean or object
     */
    public function create()
    {
        $parameters = $_POST;
        $query = "INSERT INTO " . $this->_table_name;
        $set = " VALUES(";
        $values = array('NULL');
        if (isset($parameters['uid'])) {
            echo "Not valid parameters. Exit.";
            return false;
        }
        if (!isset($parameters['email'])) {
            echo "Email required. Exit.";
            return false;
        }
        if (!isset($parameters['password'])) {
            echo "Password required. Exit.";
            return false;
        }
        if (!isset($parameters['firstname'])) {
            echo "Firstname required. Exit.";
            return false;
        }
        if (!isset($parameters['lastname'])) {
            echo "Lastname required. Exit.";
            return false;
        }
        if (!isset($parameters['password'])) {
            echo "Password required. Exit.";
            return false;
        }
        if (isset($parameters['email']) && is_string($parameters['email'])) {
            $parameters['email'] = str_replace("`", "``", $parameters['email']);
            $values[] = $this->connection->quote($parameters['email']);
        }
        if (isset($parameters['firstname']) && is_string($parameters['firstname'])) {
            $parameters['firstname'] = str_replace("`", "``", $parameters['firstname']);
            $values[] = $this->connection->quote($parameters['firstname']);
        }
        if (isset($parameters['lastname']) && is_string($parameters['lastname'])) {
            $parameters['lastname'] = str_replace("`", "``", $parameters['lastname']);
            $values[] = $this->connection->quote($parameters['lastname']);
        }
        if (isset($parameters['password']) && is_string($parameters['password'])) {
            $parameters['password'] = str_replace("`", "``", $parameters['password']);
            $values[] = $this->connection->quote(sha1($this->_config['salt'] . $parameters['password']));
        }
        $set .= implode(',', $values) . ')';
        $query .= $set;
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    /**
     * CRUD - read
     *
     * @return mixed boolean or object
     */
    public function read()
    {
        $parameters = $_GET;
        $query = "SELECT * FROM " . $this->_table_name;
        $where = " WHERE uid IS NOT null ";
        if (isset($parameters['uid']) && is_numeric($parameters['uid'])) {
            $parameters['uid'] = str_replace("`", "``", $parameters['uid']);
            $where .= "AND uid = " . $this->connection->quote($parameters['uid']);
        }
        if (isset($parameters['email']) && is_string($parameters['email'])) {
            $parameters['email'] = str_replace("`", "``", $parameters['email']);
            $where .= "AND email = " . $this->connection->quote($parameters['email']);
        }
        if (isset($parameters['firstname']) && is_string($parameters['firstname'])) {
            $parameters['firstname'] = str_replace("`", "``", $parameters['firstname']);
            $where .= "AND firstname = " . $this->connection->quote($parameters['firstname']);
        }
        if (isset($parameters['lastname']) && is_string($parameters['lastname'])) {
            $parameters['lastname'] = str_replace("`", "``", $parameters['lastname']);
            $where .= "AND lastname = " . $this->connection->quote($parameters['lastname']);
        }
        if (isset($parameters['password']) && is_string($parameters['password'])) {
            $parameters['password'] = str_replace("`", "``", $parameters['password']);
            $where .= "AND password = " . $this->connection->quote($parameters['password']);
        }
        $query .= $where;
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    /**
     * CRUD - update
     *
     * @return mixed boolean or object
     */
    public function update()
    {
        $parameters = $_PUT;
        $query = "UPDATE " . $this->_table_name;
        $set = " SET ";
        if (!isset($parameters['uid']) || !is_numeric($parameters['uid'])) {
            echo "Not valid parameters. Exit.";
            return false;
        }
        if (isset($parameters['uid']) && is_string($parameters['uid'])) {
            $parameters['uid'] = str_replace("`", "``", $parameters['uid']);
        }
        if (isset($parameters['email']) && is_string($parameters['email'])) {
            $parameters['email'] = str_replace("`", "``", $parameters['email']);
            $set .= "email = " . $this->connection->quote($parameters['email']);
        }
        if (isset($parameters['firstname']) && is_string($parameters['firstname'])) {
            $parameters['firstname'] = str_replace("`", "``", $parameters['firstname']);
            $set .= "firstname = " . $this->connection->quote($parameters['firstname']);
        }
        if (isset($parameters['lastname']) && is_string($parameters['lastname'])) {
            $parameters['lastname'] = str_replace("`", "``", $parameters['lastname']);
            $set .= "lastname = " . $this->connection->quote($parameters['lastname']);
        }
        if (isset($parameters['password']) && is_string($parameters['password'])) {
            $parameters['password'] = str_replace("`", "``", $parameters['password']);
            $set .= "password = " . $this->connection->quote(sha1($this->_config['salt'] . $parameters['password']));
        }
        $query .= $set;
        $query .= " WHERE uid = " . $this->connection->quote($parameters['uid']);
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    /**
     * CRUD - delete
     *
     * @return mixed boolean or object
     */
    public function delete()
    {
        $parameters = $_GET;
        $query = "DELETE FROM " . $this->_table_name;
        $where = " WHERE ";
        if (!isset($parameters['uid']) || !is_numeric($parameters['uid'])) {
            echo "Not valid parameters. Exit.";
            return false;
        }
        if (isset($parameters['uid']) && is_string($parameters['uid'])) {
            $parameters['uid'] = str_replace("`", "``", $parameters['uid']);
        }
        $query .= $where;
        $query .= "uid = " . $this->connection->quote($parameters['uid']);
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}
?>
