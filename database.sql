SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE restapi;

use restapi;

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `users` (`uid`, `email`, `firstname`, `lastname`, `password`) VALUES
(1, 'root@localhost.com', 'Test', 'User', 'thisisverysecret');

ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
