Simple PHP REST API implementation with CRUD operations.

Installation
------------
1. Edit config.php file and add your own database details.
2. Copy all the files into your web root
3. Start install.php (localhost/install.php). This will setup your example 
database.
4. Type localhost insto your browser. Index.php will land on read.php where you 
can see all the users from the example database.

C(reate)R(ead)U(pdate)D(elete)
-------------------------------
Create: /create.php
All database parameters are required.
Accepted methods: PUT

Read: /read.php
No required parameters.
Accepted methods: GET

Update: /update.php
Uid required.
Accepted methods: POST

Delete: /delete.php
Uid required
Accepted methods: DELETE

Known issues, missing implementations:
--------------------------------------
Authentication layer missing. There are couple of ways to implement it.
1. Cookie based authentication: Needs a permission subsystem to decide which
users can query DB accross the API.
2. Password authentication: UID 1 is able to query the API only. Needs an auth
class where you can sign in across API.
3. API key, API secret key. User should belongs to a API DB table where we can
store API details. User should provide these keys in API queries. If these are
right, they can query the API.