<?php
class DBClass
{

    private $_host = '';
    private $_username = '';
    private $_password = '';
    private $_database = '';

    public $connection;

    /**
     * Get database connection string from config.php
     * 
     * @return mixed
     */
    public function getConnection()
    {
        $config = $this->getConfig();
        $this->_host = $config['host'];
        $this->_username = $config['username'];
        $this->_password = $config['password'];
        if (strpos($_SERVER['REQUEST_URI'], '/install.php') === false) {
            $this->_database = $config['database'];
        }
        $this->connection = null;

        try{
            $this->connection = new PDO("mysql:host=" . $this->_host . ";dbname=" . $this->_database, $this->_username, $this->_password);
            $this->connection->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Error: " . $exception->getMessage();
        }

        return $this->connection;
    }

    /**
     * Get config.php array
     *
     * @return array
     *   The database config
     */
    public function getConfig() 
    {
        include_once './config.php';
        return $config;
    }
}
?>
