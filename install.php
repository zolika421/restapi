<?php
require_once './dbclass.php';
try 
{
    $dbclass = new DBClass(); 
    $connection = $dbclass->getConnection();
    $sql = file_get_contents("./database.sql"); 
    $connection->exec($sql);
    echo "Database and tables created successfully!";
    echo '<a href="index.php">Check your site</a>';
}
catch(PDOException $e)
{
    echo $e->getMessage();
}

?>
