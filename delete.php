<?php
header("Content-Type: application/json; charset=UTF-8");

require_once 'dbclass.php';
require_once 'userclass.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$user = new User($connection);

$stmt = $user->delete();
$count = $stmt->rowCount();

if ($count <= 0) {
    echo json_encode(
        array("body" => array(), "count" => 0)
    );
    return true;
}
$user = array();
$user['body'] = array();
$user['count'] = $count;
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $p  = array(
          "uid" => $uid,
          "email" => $email,
          "firstname" => $firstname,
          "lastname" => $lastname,
          "password" => $password,
    );

    array_push($user["body"], $p);
}

echo json_encode($user);
?>
